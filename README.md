<h1>Kick The Map Test Application</h1>

<ol>
   <li>
      Clone project to your folder.
   </li>
   <li>
      Run "composer install".
   </li>
   <li>
      Activate option "short_open_tag" in your php.ini
   </li>
   <li>
      Get MySQL dump in folder /web/sql_dump and import it to your data base.
   </li>
   <li>
    Use this nginx config:
      <br>
    
    server {
    charset utf-8;
    client_max_body_size 128M;
    listen 80; ## listen for ipv4

    server_name kick-the-map.loc;
    root        /path-to-app-folder/web;
    index       index.php;

    location / {
        # Redirect everything that isn't a real file to index.php
        try_files $uri $uri/ /index.php$is_args$args;

        #return 204;
    }

    location ~ ^/assets/.*\.php$ {
        deny all;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_pass 127.0.0.1:9000;
        #fastcgi_pass unix:/var/run/php5-fpm.sock;
        try_files $uri =404;
    }

    location ~* /\. {
        deny all;
    }
    }
   </li>
</ol>