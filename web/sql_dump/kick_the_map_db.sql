-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Апр 18 2022 г., 13:19
-- Версия сервера: 5.6.51
-- Версия PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `kick_the_map`
--

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `user_email` varchar(64) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `delivery_date` varchar(32) DEFAULT NULL,
  `project_name` varchar(64) DEFAULT NULL,
  `image_number` int(11) DEFAULT NULL,
  `project_success` int(11) DEFAULT NULL,
  `s3_path` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `user_email`, `project_id`, `delivery_date`, `project_name`, `image_number`, `project_success`, `s3_path`) VALUES
(12, 'jeremielocquet@gmail.com', 7511, '2022-03-25 15:43:47', 'androidRtk3', 140, 1, 's*://pathtoS3/jeremielocquet@gmail.com/jeremielocquet@gmail.com_2022-03-24_15-09-53'),
(15, 'jeremielocquet@gmail.com', 7510, '2022-03-25 15:58:51', 'androidRtk2', 39, 1, 's*://pathtoS3/jeremielocquet@gmail.com/jeremielocquet@gmail.com_2022-03-24_15-04-04'),
(16, 'b.marcaud@canaelec.com', 7524, '2022-03-28 11:28:03', 'ste foy s1 ras', 38, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_09-29-15'),
(17, 'stefano.tebaldi@gruyere-energie.ch', 7523, '2022-03-28 12:13:14', 'praBosson-20220328', 464, 1, 's*://pathtoS3/stefano.tebaldi@gruyere-energie.ch/stefano.tebaldi@gruyere-energie.ch_2022-03-28_07-04-30'),
(18, 'b.marcaud@canaelec.com', 7525, '2022-03-28 12:17:08', 'ste foy s2', 95, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_09-52-12'),
(19, 'b.marcaud@canaelec.com', 7527, '2022-03-28 12:18:36', 'ste foy s3', 63, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_10-28-17'),
(20, 'b.marcaud@canaelec.com', 7529, '2022-03-28 12:21:25', 'ste foy s4', 97, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_10-49-57'),
(21, 'b.marcaud@canaelec.com', 7530, '2022-03-28 12:23:20', 'ste foy s5', 91, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_10-55-06'),
(22, 'b.marcaud@canaelec.com', 7533, '2022-03-28 12:25:05', 'ste foy s6', 77, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_11-04-28'),
(23, 'b.marcaud@canaelec.com', 7532, '2022-03-28 12:34:43', 'ste foy s9', 73, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_11-46-26'),
(24, 'b.marcaud@canaelec.com', 7531, '2022-03-28 12:46:26', 'ste foy s8', 97, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_11-58-30'),
(25, 'b.marcaud@canaelec.com', 7534, '2022-03-28 12:47:52', 'ste foy s7', 82, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_12-12-37'),
(26, 'b.marcaud@canaelec.com', 7535, '2022-03-28 14:47:17', 'ste foy s10', 100, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_13-57-13'),
(27, 'b.marcaud@canaelec.com', 7537, '2022-03-28 15:29:30', 'ste foy s12', 59, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_15-20-40'),
(28, 'b.marcaud@canaelec.com', 7539, '2022-03-28 15:35:58', 'ste foy s13', 44, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_15-30-27'),
(29, 'b.marcaud@canaelec.com', 7544, '2022-03-28 17:39:56', 'ste foy s17', 75, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_17-30-26'),
(30, 'b.marcaud@canaelec.com', 7538, '2022-03-28 21:01:54', 'ste foy s11', 135, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_14-51-53'),
(31, 'b.marcaud@canaelec.com', 7540, '2022-03-28 21:03:50', 'ste foy s14', 115, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_15-42-22'),
(32, 'b.marcaud@canaelec.com', 7541, '2022-03-28 21:05:42', 'ste foy s15', 94, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_16-28-19'),
(33, 'b.marcaud@canaelec.com', 7543, '2022-03-28 21:07:23', 'ste foy s16', 71, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_16-54-18'),
(34, 'b.marcaud@canaelec.com', 7545, '2022-03-28 21:09:16', 'ste foy s100', 110, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_17-41-11'),
(35, 'b.marcaud@canaelec.com', 7546, '2022-03-28 21:11:17', 'ste foy s18', 101, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-28_18-02-46'),
(36, 'b.marcaud@canaelec.com', 7548, '2022-03-29 09:14:49', 'ste foy s102', 114, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_08-58-35'),
(37, 'b.marcaud@canaelec.com', 7549, '2022-03-29 09:23:21', 'ste foy s20', 144, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_09-01-32'),
(38, 'b.marcaud@canaelec.com', 7547, '2022-03-29 10:38:43', 'ste foy s19', 96, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_08-47-37'),
(39, 'ecg.dev@heig-vd.ch', 7557, '2022-03-29 10:58:42', 'test1', 314, 1, 's*://pathtoS3/ecg.dev@heig-vd.ch/ecg.dev@heig-vd.ch_2022-03-29_10-40-55'),
(40, 'ecg.dev@heig-vd.ch', 7558, '2022-03-29 10:59:05', 'test2', 202, 1, 's*://pathtoS3/ecg.dev@heig-vd.ch/ecg.dev@heig-vd.ch_2022-03-29_10-45-58'),
(41, 'b.marcaud@canaelec.com', 7559, '2022-03-29 11:30:58', 'ste foy s107 ras', 72, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_10-53-04'),
(42, 'b.marcaud@canaelec.com', 7550, '2022-03-29 11:34:36', 'ste foy s101 ras', 101, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_09-08-37'),
(43, 'b.marcaud@canaelec.com', 7551, '2022-03-29 11:36:07', 'ste foy s103', 99, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_09-30-12'),
(44, 'b.marcaud@canaelec.com', 7552, '2022-03-29 11:38:26', 'ste foy s104', 125, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_09-38-41'),
(45, 'b.marcaud@canaelec.com', 7553, '2022-03-29 11:40:09', 'ste foy s s106', 95, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_09-57-45'),
(46, 'b.marcaud@canaelec.com', 7554, '2022-03-29 11:42:29', 'ste foy s106', 95, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_10-00-08'),
(47, 'b.marcaud@canaelec.com', 7555, '2022-03-29 11:44:14', 'roullet saint estephe J1 162103132', 93, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_10-27-12'),
(48, 'b.marcaud@canaelec.com', 7556, '2022-03-29 11:45:49', 'ste foy s 21', 61, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_10-34-30'),
(49, 'b.marcaud@canaelec.com', 7560, '2022-03-29 11:48:14', 'ste foy s22', 96, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_11-08-11'),
(50, 'b.marcaud@canaelec.com', 7561, '2022-03-29 12:03:11', 'ste foy s 24 ras', 98, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_11-50-14'),
(51, 'b.marcaud@canaelec.com', 7565, '2022-03-29 12:43:13', 'ste foys108 ras', 54, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_12-12-48'),
(52, 'b.marcaud@canaelec.com', 7563, '2022-03-29 16:38:12', 'ste foy s25', 64, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_12-06-10'),
(53, 'b.marcaud@canaelec.com', 7564, '2022-03-29 16:40:13', 'ste foy s23', 75, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_12-13-32'),
(54, 'b.marcaud@canaelec.com', 7562, '2022-03-29 16:47:55', 'sb', 100, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_12-00-13'),
(55, 'b.marcaud@canaelec.com', 7566, '2022-03-29 16:52:48', 'ste foy s 33', 87, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_16-36-24'),
(56, 'b.marcaud@canaelec.com', 7574, '2022-03-29 16:59:01', 'ste foy s26', 100, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_13-56-40'),
(57, 'b.marcaud@canaelec.com', 7572, '2022-03-29 17:02:57', 'ste foy s27', 78, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_14-06-24'),
(58, 'b.marcaud@canaelec.com', 7568, '2022-03-29 17:05:46', 'ste foy s28', 64, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_14-43-44'),
(59, 'b.marcaud@canaelec.com', 7570, '2022-03-29 17:09:37', 'ste foy s109', 66, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_15-43-17'),
(60, 'b.marcaud@canaelec.com', 7573, '2022-03-29 17:24:31', 'ste foy s31', 88, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_16-14-42'),
(61, 'b.marcaud@canaelec.com', 7576, '2022-03-29 17:46:02', 'ste foy s111', 52, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_17-16-13'),
(62, 'b.marcaud@canaelec.com', 7577, '2022-03-29 17:49:03', 'ste foy s112', 57, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_17-40-32'),
(63, 'b.marcaud@canaelec.com', 7580, '2022-03-29 18:44:41', 'ste foy s 114', 84, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_18-14-20'),
(64, 'b.marcaud@canaelec.com', 7581, '2022-03-29 18:58:06', 'ste foy s115', 76, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_18-26-19'),
(65, 'kick@camandona.ch', 7578, '2022-03-29 19:41:55', 'Valors 60 29.03', 550, 1, 's*://pathtoS3/kick@Camandona.ch/kick@Camandona.ch_2022-03-29_17-48-28'),
(66, 'b.marcaud@canaelec.com', 7569, '2022-03-30 08:10:13', 'ste foy s29', 65, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_15-26-18'),
(67, 'b.marcaud@canaelec.com', 7567, '2022-03-30 08:12:37', 'ste foy s33', 99, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_16-38-03'),
(68, 'b.marcaud@canaelec.com', 7575, '2022-03-30 08:14:57', 'ste fou s110', 51, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_17-01-40'),
(69, 'b.marcaud@canaelec.com', 7579, '2022-03-30 08:21:53', 'ste foy s113', 145, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_18-22-08'),
(70, 'b.marcaud@canaelec.com', 7571, '2022-03-30 09:07:01', 'ste foy s30', 89, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-29_15-55-06'),
(71, 'ecg.dev@heig-vd.ch', 7582, '2022-03-30 10:57:10', 'test3', 87, 1, 's*://pathtoS3/ecg.dev@heig-vd.ch/ecg.dev@heig-vd.ch_2022-03-30_10-53-26'),
(72, 'ecg.dev@heig-vd.ch', 7583, '2022-03-30 11:00:33', 'test4', 190, 1, 's*://pathtoS3/ecg.dev@heig-vd.ch/ecg.dev@heig-vd.ch_2022-03-30_10-55-37'),
(73, 'travaux@bstl.ch', 7591, '2022-03-30 16:32:08', 'Hauteville eau', 231, 1, 's*://pathtoS3/travaux@bstl.ch/travaux@bstl.ch_2022-03-30_15-13-42'),
(74, 'alexismjroze@gmail.com', 7592, '2022-03-30 21:29:30', 'chv', 118, 1, 's*://pathtoS3/alexismjroze@gmail.com/alexismjroze@gmail.com_2022-03-30_16-35-08'),
(75, 'alexismjroze@gmail.com', 7595, '2022-03-31 08:38:37', 'yverdon energies', 58, 1, 's*://pathtoS3/alexismjroze@gmail.com/alexismjroze@gmail.com_2022-03-31_08-27-12'),
(76, 'alexismjroze@gmail.com', 7597, '2022-03-31 09:37:32', 'test yverdon', 40, 1, 's*://pathtoS3/alexismjroze@gmail.com/alexismjroze@gmail.com_2022-03-31_09-25-38'),
(77, 'alexismjroze@gmail.com', 7598, '2022-03-31 09:43:50', 'test2', 33, 1, 's*://pathtoS3/alexismjroze@gmail.com/alexismjroze@gmail.com_2022-03-31_09-35-00'),
(78, 'alexismjroze@gmail.com', 7599, '2022-03-31 09:49:07', 'sey', 46, 1, 's*://pathtoS3/alexismjroze@gmail.com/alexismjroze@gmail.com_2022-03-31_09-39-16'),
(79, 'ecg.dev@heig-vd.ch', 7600, '2022-03-31 11:09:26', 'calib1', 116, 1, 's*://pathtoS3/ecg.dev@heig-vd.ch/ecg.dev@heig-vd.ch_2022-03-31_11-06-47'),
(80, 'ecg.dev@heig-vd.ch', 7601, '2022-03-31 11:13:20', 'calib2', 152, 1, 's*://pathtoS3/ecg.dev@heig-vd.ch/ecg.dev@heig-vd.ch_2022-03-31_11-09-16'),
(81, 'ecg.dev@heig-vd.ch', 7602, '2022-03-31 11:15:43', 'drone', 136, 1, 's*://pathtoS3/ecg.dev@heig-vd.ch/ecg.dev@heig-vd.ch_2022-03-31_11-12-03'),
(82, 'kick@camandona.ch', 7596, '2022-03-31 11:32:13', 'TV 31.03', 321, 1, 's*://pathtoS3/kick@Camandona.ch/kick@Camandona.ch_2022-03-31_08-50-55'),
(83, 'david.dosramos@spiebatignolles.fr', 7603, '2022-03-31 11:54:43', 'dep 31/03', 425, 1, 's*://pathtoS3/david.dosramos@spiebatignolles.fr/david.dosramos@spiebatignolles.fr_2022-03-31_11-03-29'),
(84, 'florian@fb-solutions.tech', 7605, '2022-03-31 12:48:07', 'buot', 92, 1, 's*://pathtoS3/florian@fb-solutions.tech/florian@fb-solutions.tech_2022-03-31_11-36-21'),
(85, 'florian@fb-solutions.tech', 7604, '2022-03-31 12:48:48', 'buot 2', 51, 1, 's*://pathtoS3/florian@fb-solutions.tech/florian@fb-solutions.tech_2022-03-31_11-40-50'),
(86, '3d@riviera-concepts.fr', 7606, '2022-03-31 17:35:39', 'AEP VIGNASSES', 793, 1, 's*://pathtoS3/3d@riviera-concepts.fr/3d@riviera-concepts.fr_2022-03-31_10-24-35'),
(87, '3d@riviera-concepts.fr', 7609, '2022-03-31 17:39:30', 'Terragna ', 173, 1, 's*://pathtoS3/3d@riviera-concepts.fr/3d@riviera-concepts.fr_2022-03-31_14-01-46'),
(88, '3d@riviera-concepts.fr', 7589, '2022-03-31 18:12:56', '', 844, 1, 's*://pathtoS3/3d@riviera-concepts.fr/3d@riviera-concepts.fr_2022-03-30_10-22-48'),
(89, 'b.marcaud@canaelec.com', 7610, '2022-03-31 18:14:53', 'JBT1-JBT2', 115, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-03-31_15-15-09'),
(90, '3d@riviera-concepts.fr', 7608, '2022-03-31 20:52:58', 'Terragna ', 379, 1, 's*://pathtoS3/3d@riviera-concepts.fr/3d@riviera-concepts.fr_2022-03-24_13-48-37'),
(91, 'falcon.design.83@gmail.com', 7615, '2022-04-01 10:42:55', 'TestingReportScreen', 20, 1, 's*://pathtoS3/falcon.design.83@gmail.com/falcon.design.83@gmail.com_2022-04-01_10-27-58'),
(92, 'test_email_with_more_underscore@gmail.com', 7620, '2022-04-01 14:24:26', 'TestJobNonRtk197', 26, 1, 's*://pathtoS3/test_email_with_more_underscore@gmail.com/test_email_with_more_underscore@gmail.com_2022-04-01_14-22-26'),
(93, 'b.marcaud@canaelec.com', 7624, '2022-04-01 15:54:16', 'Bruges JBT B1', 96, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-04-01_15-41-02'),
(94, 'gp_ktm@geopartner.dk', 7617, '2022-04-03 16:40:35', 'pni.0104.1', 70, 1, 's*://pathtoS3/gp_ktm@geopartner.dk/gp_ktm@geopartner.dk_2022-04-01_10-49-53'),
(95, 'b.marcaud@canaelec.com', 7614, '2022-04-03 18:08:23', 'LE BUISSON DE CADOUIN', 297, 1, 's*://pathtoS3/b.marcaud@canaelec.com/b.marcaud@canaelec.com_2022-04-01_10-16-50'),
(96, 'florian@fb-solutions.tech', 7636, '2022-04-03 19:31:31', 'niort1', 282, 1, 's*://pathtoS3/florian@fb-solutions.tech/florian@fb-solutions.tech_2022-04-02_18-04-08'),
(97, 'florian@fb-solutions.tech', 7637, '2022-04-03 19:33:39', 'niort2', 172, 1, 's*://pathtoS3/florian@fb-solutions.tech/florian@fb-solutions.tech_2022-04-02_18-08-09'),
(98, 'florian@fb-solutions.tech', 7638, '2022-04-03 19:35:12', 'bagnole', 157, 1, 's*://pathtoS3/florian@fb-solutions.tech/florian@fb-solutions.tech_2022-04-02_18-11-09');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `company` varchar(128) DEFAULT NULL,
  `language` varchar(64) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `is_verified` int(4) DEFAULT NULL,
  `id_plan` int(11) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `company`, `language`, `address`, `country`, `phone`, `email`, `is_verified`, `id_plan`, `city`, `zip_code`) VALUES
(109, 'Alex', 'Test', 'English', 'De', 'New Zealand', '+648885888888', 'alexismjroze@gmail.com', 1, 3, 'Gg, De', 5239),
(1039, 'Dartus', 'Riviera Concepts', 'Français', '62 Chemin De La Campanette', 'France', '+330624430636', '3d@riviera-concepts.fr', 1, 2, 'Cagnes-sur-Mer', 6800),
(1089, 'Stefano Tebaldi', 'Gruyere Energie', 'Français', '', '', '', 'stefano.tebaldi@gruyere-energie.ch', 1, 2, '', 0),
(1224, 'Florian', 'FB Solutions', 'Français', '', 'France', '+33', 'florian@fb-solutions.tech', 1, 3, '', 0),
(2569, '', '', 'Français', '', 'Switzerland', '+41', 'travaux@bstl.ch', 0, 3, '', 0),
(2573, 'GP_Hax01', 'Geopartner', 'English', 'Rugaardsvej 55 A', 'Denmark', '+4528694032', 'gp_ktm@geopartner.dk', 0, 3, 'Odense', 5000),
(2599, 'Vuarraz Renaud', 'Camandona SA', 'Français', 'Rue de Cossonay 30', 'Switzerland', '+41797852017', 'kick@camandona.ch', 1, 2, 'Crissier', 1023),
(2639, 'ECG', 'HEIG', 'Français', 'Route de Cheseaux 1', 'Switzerland', '+41762070782', 'ecg.dev@heig-vd.ch', 1, 4, 'Yverdon', 1400);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_email` (`user_email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2640;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
