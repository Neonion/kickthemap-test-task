<?php

use yii\helpers\Url;
use app\models\Projects;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Users page</h1>

        <p>
            <form action="<?= Url::to(['site/view-user']) ?>" method="GET">
                <select onchange="this.form.submit()" name="id">
                    <option>Select user</option>
                    <?php foreach ($users as $user): ?>
                        <option value="<?= $user->id ?>"><?= $user->name ?> (<?= $user->company ?>)</option>
                    <?php endforeach; ?>
                </select>
            </form>
        </p>
    </div>

    <div class="body-content">

        <div class="row">
<!--            --><?// var_dump($currentUser); ?>
<!--            --><?// var_dump($projects); ?>

            <div class="col-lg-4">
                <h2>User Info</h2>
                <p><strong>Name: </strong><?= $currentUser->name ?></p>
                <p><strong>Company: </strong><?= $currentUser->company ?></p>
                <p><strong>Email: </strong> <?= $currentUser->email ?></p>
                <p><strong>Country: </strong><?= $currentUser->country ?></p>
                <p><strong>Phone: </strong><?= $currentUser->phone ?></p>
                <p><strong>ID_plan: </strong><?= $currentUser->id_plan ?></p>

            </div>
            <div class="col-lg-4">
                <div class="col-sm-12">
                <h2>Projects</h2>
                <table cellpadding="2" border="1">
                    <tr>
                        <td>
                            <p><strong>Date</strong></p>
                        </td>
                        <td>
                            <p><strong>Project name</strong></p>
                        </td>
                        <td>
                            <p><strong>Images</strong></p>
                        </td>
                    </tr>
                    <? $imagesCount = 0 ?>
                    <? $projectDate = 0 ?>
                    <? $price = 0 ?>
                    <?php foreach ($projects as $project): ?>
                        <?
                            if (Projects::convertToDate($projectDate) != Projects::convertToDate($project->delivery_date)) {

                                if ($imagesCount != 0) {
                                    $price += Projects::countPrice($currentUser, $imagesCount);
                                }
                                $imagesCount = 0;
                            }
                            $projectDate = Projects::convertToDate($project->delivery_date);
                            $imagesCount += $project->image_number;

                        ?>
                        <tr>
                            <td>
                                <p><?= Projects::convertToDate($project->delivery_date) ?></p>
                            </td>
                            <td>
                                <p><?= $project->project_name ?></p>
                            </td>
                            <td>
                                <p><?= $project->image_number ?></p>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <? $price += Projects::countPrice($currentUser, $imagesCount); ?>
                    </tr>
                </table>
                </div>


            </div>
            <div class="col-lg-4">
                <h2>Total earnings</h2>
                <p><?= $price ?>$</p>
            </div>
        </div>

    </div>
</div>
