<div class="site-error">

    <h1>XML parsing result</h1>

    <div class="alert alert-success">
        Success!
    </div>

    <p>
        XML file was successfully parsed.
    </p>
    <p>
        Please, check Data base.
    </p>

</div>
