<div class="site-error">

    <h1>XML parsing result</h1>

    <div class="alert alert-error">
        Error!
    </div>

    <p>
        XML file was not found or there is an error while parsing data.
    </p>
    <p>
        Please, check your XML file.
    </p>

</div>
