<?php

namespace app\controllers;

use app\models\Projects;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Users;

class SiteController extends Controller
{
    /**
     * Displays user select.
     *
     * @return string
     */
    public function actionIndex()
    {
        $users = Users::find()->all();

        return $this->render('index', ['users' => $users]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionViewUser($id)
    {
        var_dump($id);
        $users = Users::find()->all();
        $user = Users::findOne(['id' => $id]);
        $projects = Projects::find()->where(['user_email' => $user->email])->orderBy(['delivery_date' => SORT_DESC])->all();

        return $this->render('view_user', [
            'currentUser' => $user,
            'projects' => $projects,
            'users' => $users
        ]);
    }
}
