<?php

namespace app\controllers;

use app\models\Projects;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class XmlParserController extends Controller
{
    /**
     * Pars users.
     *
     * @return string
     */
    public function actionParsUsers()
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/xml/Users.xml')) {
            $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . '/xml/Users.xml');
        } else {
            exit('Cant open XML file');
        }

        foreach ($xml->children() as $row) {
            $data_array = (array)$row;

            $id = $data_array['field'][0];
            $name = (string)$data_array['field'][1];
            $company = $data_array['field'][2];
            $language = $data_array['field'][3];
            $address = $data_array['field'][4];
            $country = $data_array['field'][5];
            $phone = $data_array['field'][6];
            $email = $data_array['field'][7];
            $is_verified = (int)$data_array['field'][8];
            $id_plan = $data_array['field'][9];
            $city = $data_array['field'][10];
            $zip_code = (int)$data_array['field'][11];

            $users = new Users();
            $users->id = $id;
            $users->name = $name;
            $users->company = $company;
            $users->language = $language;
            $users->address = $address;
            $users->country = $country;
            $users->phone = $phone;
            $users->email = $email;
            $users->is_verified = $is_verified;
            $users->id_plan = $id_plan;
            $users->city = $city;
            $users->zip_code = $zip_code;
            $users->save(false);
        }

        return $this->render('success');
    }

    /**
     * Pars projects.
     *
     * @return string
     */
    public function actionParsProjects()
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/xml/Projects.xml')) {
            $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . '/xml/Projects.xml');
        } else {
            return $this->render('error');
        }

        foreach ($xml->children() as $row) {
            $data_array = (array)$row;

            $id = $data_array['field'][0];
            $user_email = (string) $data_array['field'][1];
            $project_id = $data_array['field'][2];
            $delivery_date = $data_array['field'][3];
            $project_name = $data_array['field'][4];
            $image_number = (int) $data_array['field'][5];
            $project_success = $data_array['field'][6];
            $s3_path = $data_array['field'][7];

            $projects = new Projects();
            $projects->id = $id;
            $projects->user_email = $user_email;
            $projects->project_id = $project_id;
            $projects->delivery_date = $delivery_date;
            $projects->project_name = $project_name;
            $projects->image_number = $image_number;
            $projects->project_success = $project_success;
            $projects->s3_path = $s3_path;
            $projects->save(false);
        }

        return $this->render('success');
    }
}
