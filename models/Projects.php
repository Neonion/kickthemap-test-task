<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string|null $user_email
 * @property int|null $project_id
 * @property string|null $delivery_date
 * @property string|null $project_name
 * @property int|null $image_number
 * @property int|null $project_success
 * @property string|null $s3_path
 *
 * @property Users[] $users
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'image_number', 'project_success'], 'integer'],
            [['user_email', 'project_name'], 'string', 'max' => 64],
            [['delivery_date'], 'string', 'max' => 32],
            [['s3_path'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_email' => 'User Email',
            'project_id' => 'Project ID',
            'delivery_date' => 'Delivery Date',
            'project_name' => 'Project Name',
            'image_number' => 'Image Number',
            'project_success' => 'Project Success',
            's3_path' => 'S 3 Path',
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['email' => 'user_email']);
    }

    static public function convertToDate($dateTime)
    {
        $time = $dateTime;

        return date("Y-m-d",strtotime($time));
    }

    static public function countPrice($currentUser, $imagesCount)
    {
        $idPlan = $currentUser->id_plan;
        $price = 0;

        switch ($idPlan) {
            case '2':
                $flatRate = 2;
                $maxImages = 200;
                break;
            case '3':
                $flatRate = 3;
                $maxImages = 300;
                break;
            case '4':
                $flatRate = 4;
                $maxImages = 400;
                break;
            default:
                $flatRate = 0;
                $maxImages = 0;
                break;
        }

        if ($imagesCount == $maxImages) {
            $price = $flatRate;
        } elseif ($imagesCount > $maxImages) {
            $price = $flatRate;
            $price += ($imagesCount - $maxImages) * 0.01;

        } elseif ($imagesCount < $maxImages) {
            $price = $imagesCount * $flatRate / $maxImages;
        }

        return $price;
    }
}
