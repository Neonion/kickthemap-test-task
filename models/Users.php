<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $company
 * @property string|null $language
 * @property string|null $address
 * @property string|null $country
 * @property string|null $phone
 * @property string|null $email
 * @property int|null $is_verified
 * @property int|null $id_plan
 * @property string|null $city
 * @property int|null $zip_code
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_verified', 'id_plan', 'zip_code'], 'integer'],
            [['name', 'company', 'city'], 'string', 'max' => 128],
            [['language', 'country', 'email'], 'string', 'max' => 64],
            [['address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'company' => 'Company',
            'language' => 'Language',
            'address' => 'Address',
            'country' => 'Country',
            'phone' => 'Phone',
            'email' => 'Email',
            'is_verified' => 'Is Verified',
            'id_plan' => 'Id Plan',
            'city' => 'City',
            'zip_code' => 'Zip Code',
        ];
    }
}
