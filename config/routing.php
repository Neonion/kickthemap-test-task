<?php
return [
    '/' => 'site/index',
    'view-user' => 'site/view-user',
    'xml/pars-users' => 'xml-parser/pars-users',
    'xml/pars-projects' => 'xml-parser/pars-projects',

    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
];